This `workflow template is derived from rnaseq-deseq2 <https://gitlab.lcsb.uni.lu/aurelien.ginolhac/snakemake-rna-seq>` and Johannes Koester Snakemake work.  
Taxonomic assignment is obtained using Silva 138.1, McLaren, M. R., & Callahan, B. J. (2021). Silva 138.1 prokaryotic SSU taxonomic training data formatted for DADA2 [Data set](https://doi.org/10.5281/zenodo.4587955)
