Volcano plot of differential expression test. Taxa with adjusted-pval < 0.05 are colored by direction. Taxa names are shown for top 10 logFC taxa (within adjusted-pval < 0.05), 
 abbreviated as f(amily).genus

