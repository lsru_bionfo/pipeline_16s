Table of differential expression results (GLM-CLR) for all tested genera, including prevalence. "padj" are BH-adjusted p-values.
