
rule session:
    output:
        report("logs/session.txt", category = "Versions", caption = "../report/session.rst")
    log:
        "logs/session.log"
    script:
        "../scripts/session.R"

