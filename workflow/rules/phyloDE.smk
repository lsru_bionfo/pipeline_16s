

rule phyloseq_init:
    input:
        seqtabnochim = "dada2/seqtab_nochim.rds",
        taxa="dada2/taxa.rds"
    output:
        phyloall = "phyloseq/phyloseq.rds",
        phylogenus = "phyloseq/phyloseq_genus.rds"
    params:
        samples = config["samples"]
    log:
        "logs/phyloseq_init.log"
    script:
        "../scripts/phyloseq_init.R"


rule pca:
    input:
        "phyloseq/phyloseq_genus.rds"
    output:
        PCoA_groups=report("phyloseq/PCoA_groups.pdf", category = "Explore", caption = "../report/PCoA_groups.rst"),
        PCoA_cage=report("phyloseq/PCoA_cage.pdf", category = "Explore", caption = "../report/PCoA_cage.rst"),
        Profile=report("phyloseq/top_taxa.pdf", category = "Explore", caption = "../report/top_taxa.rst")
    log:
        "logs/pca.log"
    script:
        "../scripts/pcoa_profile.R"


rule diffexp:
    input:
        "phyloseq/phyloseq_genus.rds"
    output:
        clrdata = report("diffexp/{contrast}/clrdata.csv", category = "Differential Expression", caption = "../report/clr.rst"),
        table = report("diffexp/{contrast}/DE_clr.csv", category = "Differential Expression", caption = "../report/diffexp.rst"),
        plot_count = report("diffexp/{contrast}/Plot_Expression.pdf", category = "Differential Expression", caption = "../report/pc.rst"),
        volcano = report("diffexp/{contrast}/Volcano.png", category = "Differential Expression", caption = "../report/volcano.rst"),
        heatmap = report("diffexp/{contrast}/res_heatmap.png", category = "Differential Expression", caption = "../report/heatmap.rst")
    params:
        contrast="{contrast}"
    message:
        "performing differential expression"
    log:
        "logs/diffexp/{contrast}/diffexp.log"
    threads: get_deseq2_threads()
    script:
        "../scripts/DE.R"

