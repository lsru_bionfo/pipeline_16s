reference = config["reference"]

rule trimlimits:
    input:
        units = config["units"]
    output:
        qualprofile = report("dada2/Qual_profile_test.png", category = "QC", caption = "../report/profileplot.rst"),
        trunclen = "dada2/trimchoices.tsv"
    params:
        amplicon = config["amplicon"]
    log:
        "logs/trimlimits.log"
    script:
        "../scripts/trim_limits.R"

rule count:
    input:
        units = config["units"],
        trunclen = "dada2/trimchoices.tsv"
    output:
        errplot = "dada2/errFR.pdf",
        seqtabnochim = "dada2/seqtab_nochim.rds",
        summary = "dada2/summary_dada2.tsv",
        summaryplot=report("dada2/summaryplot.png", category = "QC", caption = "../report/summaryplot.rst")
    params:
        cpus = config["cpus"]
    log:
        "logs/dada2.log"
    script:
        "../scripts/dada2.R"
        
rule taxonomy:
    input:
        seqtabnochim = "dada2/seqtab_nochim.rds",
        silvafile = expand("silva/{reference}", reference = reference)
    output:
        taxa = "dada2/taxa.rds"
    params:
        reference = config["reference"]
    log:
        "logs/taxo.log"
    script:
        "../scripts/taxo.R"
