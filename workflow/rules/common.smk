import pandas as pd
import numpy as np
from snakemake.utils import validate, min_version
##### set minimum snakemake version #####
min_version("5.20.1") # for singularity binds
from snakemake.utils import validate, min_version

##### load config and sample sheets #####
configfile: "config/config.yaml"
#validate(config, schema="../schemas/config.schema.yaml")

samples = pd.read_table(config["samples"]).set_index("sample", drop=False)
#validate(samples, schema="../schemas/samples.schema.yaml")

# check if contrast condition in samples and config match
conditions = np.unique(samples["group"].values).tolist()
for key, val in config["diffexp"]["contrasts"].items():
  for cond in val:
    assert cond.find('-') == -1, "dashes found in {}, not allowed since later converted to underscores by DESeq2".format(cond)
    assert cond in conditions, "condition {} in config.yaml don't match in samples.yaml".format(cond)
  assert len(key.split("_vs_")) == 2, "contrasts in config.yaml must in the form XX_vs_YY (provided: {})".format(key)
  for cond in key.split("_vs_"):
    assert cond in conditions, "conditions in headers must be present in samples.yaml ({} missing)".format(cond)

wildcard_constraints:
    sample="|".join(samples["sample"])
    
def get_final_output():
    final_output = expand(["diffexp/{contrast}/clrdata.csv",
    "diffexp/{contrast}/DE_clr.csv",
        "diffexp/{contrast}/Volcano.png",
        "diffexp/{contrast}/res_heatmap.png",
        "diffexp/{contrast}/Plot_Expression.pdf"],
        contrast=config["diffexp"]["contrasts"]
    )
    final_output.append("dada2/Qual_profile_test.png")  
    final_output.append("dada2/summaryplot.png")  
    final_output.append("phyloseq/PCoA_groups.pdf")
    final_output.append("phyloseq/PCoA_cage.pdf")
    final_output.append("phyloseq/top_taxa.pdf")
    final_output.append("logs/session.txt")
    return final_output

def get_deseq2_threads(wildcards=None):
    # https://twitter.com/mikelove/status/918770188568363008
    few_coeffs = False if wildcards is None else len(get_contrast(wildcards)) < 10
    return 1 if len(samples) < 100 or few_coeffs else 6

def get_contrast(wildcards):
    return config["diffexp"]["contrasts"][wildcards.contrast]
