reference = config["reference"]

rule get_annotation:
    output:
       expand("silva/{reference}", reference = reference)
    params:
       reference = config["reference"]
    message: "downloading {output} "
    shell:
        """
        mkdir -p silva
        wget -qnc --no-check-certificate '{reference}' -O '{output}' 
        """
