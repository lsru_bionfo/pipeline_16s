
## 16s V3-V4 Illumina analysis with Snakemake, from fastq to differential expression

[Snakemake](https://snakemake.readthedocs.io/en/stable/) is a workflow manager system for
creating reproducible and scalable pipelines. See a < 2 min 
[video presentation](https://www.youtube.com/watch?v=UOKxta3061g) that scheme the main principles.

This repository allows you to perform a *Amplicon-seq analysis*, from raw reads to differential expression and is customised for V3-V4 Illumina kit using as parent template the snakemake workflow from [Aurelien Ginolhac](https://gitlab.lcsb.uni.lu/aurelien.ginolhac/snakemake-rna-seq).
Dependencies between steps are depicted below:

![](dag.png)

Reads can be supplied as **paired-end**.

### Tools used

- Analyses are performed in [R](https://cran.r-project.org) version 4.3.3. 

- Fastq files are processed to counts using [dada2](https://benjjneb.github.io/dada2/tutorial.html). Limits for fastq trimming are first estimated using a loess fit on the quality of 5 sampled fastq pairs. The limits are chosen to keep an average quality of 30. Limit of R1 file is extended if needed to keep an overlap of 20 bases. 

- Taxonomy is by default based on Silva database, in its version formatted for [dada2](https://zenodo.org/records/4587955). The reference file is fetched if not present and ASVs are annotated up to genus level. 

- Further analysis is performed using [phyloseq](https://joey711.github.io/phyloseq/) and [microbiome](https://www.bioconductor.org/packages/release/bioc/html/microbiome.html) packages. Data are formatted as a pseq object and sequences with absent or "uncharacterized" Phylum are filtered out. Data are then aggregated to genus level for further analyses. Both the raw count and the genus-aggregated count objects are exported. 

- Exploratory graphs are produced:
 - PCoA on Bray diversity of samples showing groups
 - If cage information is present, a PCoA on Bray diversity coloring for them is also exported. 
 - The major taxa present in samples can be checked in a profile plot of the major taxa present.

- Differential expression analysis between groups is performed by using a GLM model on CLR transformed data. Output include table of results, Table of expression values in CLR unit, a volcano plot, an expression plot of the most significant taxa, and a heatmap of significant taxa if any.

All software are available in a [docker image](https://hub.docker.com/repository/docker/anthoulagaigneaux/snakemake_16s). 
`Snakemake` will automatically fetch the image and convert it to a [Singularity](https://sylabs.io/) image and cache it until the tag change (or image is deleted from the `.snakemake/singularity/` folder.


### Install

The installation is the same as for the [parent snakemake](https://gitlab.lcsb.uni.lu/aurelien.ginolhac/snakemake-rna-seq)

On the HPC [iris](https://hpc.uni.lu/systems/iris/), what is needed is

- `snakemake`. version **at least** `5.20.1` as an [issue](https://github.com/snakemake/snakemake/issues/193) prevents the binding of folders (`5.9.1` was fine).
- `singularity`
- This workflow template

For `snakemake`, you could install it via `conda`

and installing `snakemake` as in the [RFTM](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html)

#### Install `snakemake`

- Follow the instructions from *Sarah Peter* in her [tutorial](https://ulhpc-tutorials.readthedocs.io/en/latest/bio/snakemake/#env) for installing `Miniconda3`. If you run into permission issues check out this [tutorial](https://ginolhac.github.io/chip-seq/snakemake/).
- Once your `.bashrc` is modified and sourced, you should see `(base)` at the left part of your prompt
- Install `mamba` as recommended by J. Köster: `conda install -c conda-forge mamba`
- Install `snakemake` in an environment of the same name` mamba create -c conda-forge -c bioconda -n snakemake snakemake`

The last command will install `snakemake` in a specific environment `snakemake`.

Of note, I am here following *Johannes Köster* advice to leave `conda` alone in `(base)` and install packages into a dedicated environment 
like `snakemake` for me.

##### Update `snakemake`

Once on a node, and the `(base)` *conda* activated, you can update your `snakemake` version with:

`conda update -c conda-forge -c bioconda -n snakemake snakemake`

#### Fetch the workflow template

```bash
VERSION="v0.0.3"
wget -qO- https://gitlab.lcsb.uni.lu/lsru_bionfo/pipeline_16s/-/archive/${VERSION}/pipeline_16s-.tar.gz | tar xfz - --strip-components=1
```

This command will download, extract (without the root folder) the following files

```
config/
Dockerfile
LICENSE
README.md
workflow/
```

You may want to delete the `LICENSE`, `Dockerfile`, `CHANGELOG.md` and `README.md` if you wish, 
they are not used by `snakemake`

#### Fetch the test data

```bash
wget -qO- https://webdav-r3lab.uni.lu/public/DLSM/sample_16s.tar.gz | tar xvfz -
```

You should obtain the following 16 FASTQ gzipped files in 2 folders, representing 8 samples

```
├── fastgr1
│   ├── S1_T0_m1_L001_R1_001.fastq.gz
│   ├── S1_T0_m1_L001_R2_001.fastq.gz
│   ├── S2_T0_m2_L001_R1_001.fastq.gz
│   ├── S2_T0_m2_L001_R2_001.fastq.gz
│   ├── S3_T0_m3_L001_R1_001.fastq.gz
│   ├── S3_T0_m3_L001_R2_001.fastq.gz
│   ├── S4_T0_m4_L001_R1_001.fastq.gz
│   └── S4_T0_m4_L001_R2_001.fastq.gz
├── fastgr2
│   ├── S1_T1_m1_L001_R1_001.fastq.gz
│   ├── S1_T1_m1_L001_R2_001.fastq.gz
│   ├── S2_T1_m2_L001_R1_001.fastq.gz
│   ├── S2_T1_m2_L001_R2_001.fastq.gz
│   ├── S3_T1_m3_L001_R1_001.fastq.gz
│   ├── S3_T1_m3_L001_R2_001.fastq.gz
│   ├── S4_T1_m4_L001_R1_001.fastq.gz
│   └── S4_T1_m4_L001_R2_001.fastq.gz
```

Finally, you file structure should look like

```
.
├── config
│   ├── config.yaml
│   ├── samples.tsv
│   └── units.tsv
├── fastgr1
│   ├── S1_T0_m1_L001_R1_001.fastq.gz
│   ├── S1_T0_m1_L001_R2_001.fastq.gz
│   ├── S2_T0_m2_L001_R1_001.fastq.gz
│   ├── S2_T0_m2_L001_R2_001.fastq.gz
│   ├── S3_T0_m3_L001_R1_001.fastq.gz
│   ├── S3_T0_m3_L001_R2_001.fastq.gz
│   ├── S4_T0_m4_L001_R1_001.fastq.gz
│   └── S4_T0_m4_L001_R2_001.fastq.gz
├── fastgr2
│   ├── S1_T1_m1_L001_R1_001.fastq.gz
│   ├── S1_T1_m1_L001_R2_001.fastq.gz
│   ├── S2_T1_m2_L001_R1_001.fastq.gz
│   ├── S2_T1_m2_L001_R2_001.fastq.gz
│   ├── S3_T1_m3_L001_R1_001.fastq.gz
│   ├── S3_T1_m3_L001_R2_001.fastq.gz
│   ├── S4_T1_m4_L001_R1_001.fastq.gz
│   └── S4_T1_m4_L001_R2_001.fastq.gz
└── workflow
    ├── report
    │   ├── clr.rst
    │   ├── diffexp.rst
    │   ├── heatmap.rst
    │   ├── PCoA_cage.rst
    │   ├── PCoA_groups.rst
    │   ├── pc.rst
    │   ├── profileplot.rst
    │   ├── session.rst
    │   ├── summaryplot.rst
    │   ├── top_taxa.rst
    │   ├── volcano.rst
    │   └── workflow.rst
    ├── rules
    │   ├── common.smk
    │   ├── dada.smk
    │   ├── phyloDE.smk
    │   ├── qc.smk
    │   └── ref.smk
    ├── scripts
    │   ├── dada2.R
    │   ├── DE.R
    │   ├── pcoa_profile.R
    │   ├── phyloseq_init.R
    │   ├── session.R
    │   ├── taxo.R
    │   └── trim_limits.R
    └── Snakefile
```


#### Singularity

This is the easy part as the [HPC team](https://hpc.uni.lu/about/team.html) is providing us with available [modules](https://hpc.uni.lu/users/software/)

The only needed command once on a `node` is:

```bash
module load tools/Singularity
```

Check the tags of the Docker images on [Docker Hub](https://hub.docker.com/r/anthoulagaigneaux/snakemake_16s).
The current tag used is mentionned in `workflow/Snakefile`. 

#### Reporting in publications

Both release tag for the template and for the Docker images need to be reported along with the URL to this page

#### Aliases

Since we need some setup each time, you can have those aliases in your `.bashrc`

```bash
alias smk='mamba activate snakemake && module load tools/Singularity'
alias dag='snakemake --dag | dot -Tpng > dag.png'
complete -o bashdefault -C snakemake-bash-completion snakemake
```

Once on a `node`, you can quickly activate the right `conda` environment and load `Singularity`.
`dag` is useful with shortly get the pdf of the dependencies task.
Lastly, the `complete` instruction is for the auto-completion in bash.


### Usage

Book resources on `iris`, like 12 cores for 1 hour with:

```bash
srun --cpu-bind=none -p interactive -t 0-1:00:0 -N 1 -c 12 --ntasks-per-node=1 --pty bash -i
```

Once on a `node` you can use the alias `smk` to activate the environment and load `Singularity`

### Test mouse data

#### Task dependencies: [Directed Acyclic Graph (DAG)](https://en.wikipedia.org/wiki/Directed_acyclic_graph)


Now, we have one hour to run the workflow. By default, `snakemake` will find automatically the `Snakefile` present in `workflow/`.

First of all, compute the dependency graph:

`dag` the alias of `snakemake --dag | dot -Tpdf > dag.pdf`

you should obtain the following:

![](dag.png)

All *plain* lines mean that the task is not complete, they will be *dashed* when achieved.

As tabulated-text, the same output is obtained with `snakemake --summary` (here a few entries for display purposes)

```
dada2/errFR.pdf -       count   -       logs/dada2.log  missing update pending
dada2/seqtab_nochim.rds -       count   -       logs/dada2.log  missing update pending
dada2/summary_dada2.tsv -       count   -       logs/dada2.log  missing update pending
dada2/summaryplot.png   -       count   -       logs/dada2.log  missing update pending
dada2/Qual_profile_test.png     -       trimlimits      -       logs/trimlimits.log     missing update pending
[...]
```

Once some files are obtained, the missing files are filling up.

#### Running the workflow

A `dry-run` to start with is recommended:

```bash
snakemake -c12 --use-apptainer --singularity-args "-B /mnt/irisgpfs/projects/lsru/16S/190503/:/fastq" -n
```
- we booked *12* cores, tell `snakemake` about it with `c12`.
- the `--use-apptainer` option indicates `snakemake` to fetch and use the docker image linked in the `Snakefile`.
- the `--singularity-args` bind the parent location of all fastq files if they are in a different (and upstream) directory than the current working directory. It can be omitted when fastq folders are in current working directory.
- finally `-n` is the `dry-run flag.

A long list of _green_ instructions should be printed out, and finally in _yellow_ the summary:

```
Job stats:
job               count
--------------  -------
all                   1
count                 1
diffexp               1
get_annotation        1
pca                   1
phyloseq_init         1
session               1
taxonomy              1
trimlimits            1
total                 9
This was a dry-run (flag -n). The order of jobs does not reflect the order of execution.
```

**9 tasks** need to be run. Now run the same command, *without* the `-n` flag.

If all goes well (with 12 cores, it should take ~ 5-10 min), which is: no red text appears, you can generate the final report using:

```bash
snakemake --report
```

### Expected outputs

- The `snakemake` report: `report.html`, download a [copy here]()

In the report, the different versions of used are in `session.txt`.

### Writing units.tsv files

`units.tsv` file links the samples to their fastqs files. Both R1 and R2 fastq files for each sample should be present in the same directory. 

The supplied template describes an experiment: the field `directory` indicates where to find the files.  Both R1 and R2 fastq files for each sample should be present in the same directory. 
So, after alignment of tabulated fields, it looks like this:

```
sample	  directory	  fq1	                          fq2
S1_T0_m1	fastgr1	    S1_T0_m1_L001_R1_001.fastq.gz	S1_T0_m1_L001_R2_001.fastq.gz
S2_T0_m2	fastgr1	    S2_T0_m2_L001_R1_001.fastq.gz	S2_T0_m2_L001_R2_001.fastq.gz
S3_T0_m3	fastgr1	    S3_T0_m3_L001_R1_001.fastq.gz	S3_T0_m3_L001_R2_001.fastq.gz
S4_T0_m4	fastgr1	    S4_T0_m4_L001_R1_001.fastq.gz	S4_T0_m4_L001_R2_001.fastq.gz
```

### Writing samples.tsv files

`samples.tsv` file links the samples to their group and possibly cage if applicable. 

The sample names will be used throughout the analysis. They **need** to be syntactically correct (no empty space, no start with a number, no parentheses). It is also advised to choose them :
 - Short or at least of a comparable length
 - Indicative of a group
 - Unique
 - Ideally, as you would name it when uploading data to a public database.

The cage column is optional. When present, a PCoA plot is produced to check possible sample grouping by cage.

The file should look like this:

```
sample	group	cage
S1_T0_m1	Gr1	1
S2_T0_m2	Gr1	1
S3_T0_m3	Gr1	2
S4_T0_m4	Gr1	2
S1_T1_m1	Gr2	1
S2_T1_m2	Gr2	1
S3_T1_m3	Gr2	2
S4_T1_m4	Gr2	2
```

### Cleaning all output

Cleaning output files, here with 2 jobs can be performed with:

```bash
snakemake -j2 --delete-all-output
```

Of note, this does not remove the folders, only the files.

### Slurm launcher script

As an example, to be adapted

```
#!/bin/bash -l
#SBATCH -N 1
#SBATCH -J XX
#SBATCH --mail-type=begin,end,fail
#SBATCH --mail-user=xx@xx.x
#SBATCH --ntasks-per-node=1
#SBATCH --mem-per-cpu=4096
#SBATCH -c 24
#SBATCH --time=0-07:30:00
#SBATCH -p batch

export SRUN_CPUS_PER_TASK=$SLURM_CPUS_PER_TASK

module load tools/Singularity

conda activate snakemake

srun snakemake --cores "${SLURM_CPUS_PER_TASK}" --use-apptainer --singularity-args "-B /mnt/irisgpfs/projects/lsru/16S/190503/:/fastq"

```

### Limitations

- The fastqc and multiqc are not included as the workflow is mostly intended for analysis of fastq from different folders (sources, batches), which already underwent qc.

- The trimming limits are based on an amplicon size of 460. This can be adapted in the config.yaml file.

- When aggregating to genus level, all taxa with genus not defined are pruned (parameter `NArm = TRUE` in from tax_glom)

