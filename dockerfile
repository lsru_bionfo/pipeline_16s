
FROM eddelbuettel/r2u:jammy

# built using docker build -t snakemake_16s . 2> log.txt

LABEL version="0.1"  
LABEL description="Docker image to build the snakemake 16s singularity"

ARG DEBIAN_FRONTEND=noninteractive

# add-apt-repository -y ppa:openjdk-r/ppa && \
RUN apt-get update && apt-get install -y software-properties-common && \
    apt-get update && apt-get install -y \
        build-essential \
        cmake \
        curl \
        gpg-agent \
        libboost-all-dev \
        libbz2-dev \
        libcurl4-openssl-dev \
        liblzma-dev \
        libncurses5-dev \
        libssl-dev \
        libxml2-dev \
        libgd-perl libgd-graph-perl \
        openjdk-8-jdk \
        python-is-python3 \
        python-dev-is-python3 \
        python3-pip \
        unzip \
        vim-common \
        wget \
        graphviz \
        zlib1g-dev

# default to python3 
# better to use https://stackoverflow.com/a/50331137/1395352
RUN ln -sf /usr/bin/python-config /usr/bin/python3.10-config && ln -sf /usr/bin/python3 /usr/bin/python

# python modules
RUN python3 -m pip install snakemake snakemake-wrapper-utils multiqc pandas pulp==2.7.0
#https://github.com/snakemake/snakemake/issues/2607

# R is R version 4.3.3, Bioconductor 3.18
# dada2 is 1.30.0
# phyloseq is 1.46.0
RUN install.r  tidyverse RColorBrewer viridis pheatmap cowplot janitor broom ggrepel \
phyloseq microbiome ComplexHeatmap  dada2 && \
  installGithub.r gmteunisse/fantaxtic gmteunisse/ggnested
