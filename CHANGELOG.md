
# v0.0.3 2025-02-25

- add a changelog
- update R to 4.4 and add decipher and phangorn
- fix summaryplot height
- add full path to ref for more flexibility
